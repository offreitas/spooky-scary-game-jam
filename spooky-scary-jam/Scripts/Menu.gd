extends Control

signal go_to_game()

func _ready():
	$Credits_Container.visible      = false
	$Instructions_Container.visible = false

func _on_Credits_pressed():
	$Main_Menu_Container.visible = false
	$Credits_Container.visible   = true

func _on_BACK_pressed():
	$Main_Menu_Container.visible = true
	$Credits_Container.visible   = false

func _on_Start_pressed():
	$Main_Menu_Container.visible    = false
	$Instructions_Container.visible = true
	
	$Timer.start()

func _on_Exit_pressed():
	self.get_tree().quit()

func _on_Timer_timeout():
	emit_signal("go_to_game")

func _on_Skip_pressed():
	emit_signal("go_to_game")
