extends Node2D

export(float) var atk := 10

signal dmg(atk, target)

func _ready():
	if !get_parent().get_node("AnimatedSprite").is_flipped_h():
		$AnimationPlayer.play("Sword_Atk")
	else:
		$AnimationPlayer.play("Sword_Atk_Inv")

func _on_Area2D_body_entered(body):
	if body.is_in_group("Enemy"):
		emit_signal("dmg", atk, body)

func _on_AnimationPlayer_animation_finished(_anim_name):
	self.call_deferred("free")
