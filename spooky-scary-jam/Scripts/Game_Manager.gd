extends Node2D

export(PackedScene) var life_scene
export(PackedScene) var player_scene
export(PackedScene) var note_scene
export(PackedScene) var picker_scene
export(PackedScene) var orcs_wave_scene
export(PackedScene) var undeads_wave_scene
export(PackedScene) var demons_wave_scene
export(PackedScene) var map_scene
export(PackedScene) var orcs_music_scene
export(PackedScene) var undeads_music_scene
export(PackedScene) var demons_music_scene

var map_nav2d       : Navigation2D
var enemy_positions : Array
var player_pos      : Vector2
var note_spawn      : Vector2 = Vector2(-32, 656)
var note_offset     : int     = 4

enum areas{ORCS, UNDEADS, DEMONS}
var cur_area = areas

var wave_enemies : Array
var cur_wave     : String
var cur_music    : String
var cur_pointers : String = "Orcs_Spawn"
var cur_spb      : float

var undeads_door : String = "Undeads_Closed"
var demons_door  : String = "Demons_Closed"
var undeads_area : Area2D
var demons_area  : Area2D

var forward_cam  : Area2D
var backward_cam : Area2D
var upward_cam   : Area2D
var downward_cam : Area2D

signal game_over()

func _ready():
	var life        = life_scene.instance()
	var map         = map_scene.instance()
	var orcs_music  = orcs_music_scene.instance()
	var picker      = picker_scene.instance()
	var player      = player_scene.instance()
	var orcs_wave   = orcs_wave_scene.instance()
	
	var Notes : Node2D = Node2D.new()
	
	var spawn_points = map.get_node(cur_pointers).get_children()
	
	Notes.set_name("Notes")
	
	map.set_name("Map")
	map_nav2d = map.get_node("Navigation2D")
	
	cur_music = "Orcs_Music"
	orcs_music.set_name(cur_music)
	orcs_music.connect("create_note", self, "_on_create_note")
	orcs_music.connect("finished", self, "_on_finished")
	cur_spb = orcs_music._get_spb()
	
	picker.set_name("Picker")
	picker.set_position(Vector2(512, 656))
	
	life.set_name("Life")
	life.connect("hero_dead", self, "_on_hero_death")
	
	player.set_name("Hero")
	player.set_position(Vector2(512, 360))
	player._setup(orcs_music._get_wait_time())
	player.connect("update_hero_pos", self, "_on_update_hero_pos")
	player.connect("hero_damaged", self, "_on_hero_damaged")
	
	cur_area = areas.ORCS
	cur_wave = "Orcs_Wave"
	orcs_wave.set_name(cur_wave)
	orcs_wave.connect("change_wave", self, "_on_change_wave")
	orcs_wave.connect("end_wave", self, "_on_wave_end")
	
	self.add_child(orcs_music)
	self.add_child(map)
	self.add_child(picker)
	self.add_child(life)
	self.add_child(player)
	self.add_child(Notes)
	
	undeads_area = $Map.get_node("Undeads_Area")
	# warning-ignore:return_value_discarded
	undeads_area.connect("body_entered", self, "_on_spawn_area")
	
	demons_area = $Map.get_node("Demons_Area")
	# warning-ignore:return_value_discarded
	demons_area.connect("body_entered", self, "_on_spawn_area")
	
	forward_cam = $Map.get_node("Forward_Camera")
	# warning-ignore:return_value_discarded
	forward_cam.connect("body_exited", self, "_move_cam_x")
	forward_cam.monitorable = true
	
	upward_cam = $Map.get_node("Upward_Camera")
	# warning-ignore:return_value_discarded
	upward_cam.connect("body_exited", self, "_move_cam_y")
	upward_cam.monitorable = true
	
	orcs_music._play_with_offset(note_offset)
	
	for i in range(spawn_points.size()):
		enemy_positions.push_back(spawn_points[i])
	
	orcs_wave._setup(enemy_positions, $Hero, map_nav2d, cur_spb)
	
	self.add_child(orcs_wave, 0)
	
	get_node(cur_wave)._wave1()
	_update_children()

	for i in range(wave_enemies.size()):
		wave_enemies[i].connect("enemy_dead", self, "_on_enemy_death")
		wave_enemies[i]._update($Hero.get_global_position())

func _input(event):
	if event.is_action_pressed("ui_up") or event.is_action_pressed("ui_down") or event.is_action_pressed("ui_right") or event.is_action_pressed("ui_left") or event.is_action_pressed("attack"):
		$Picker/Area2D.monitorable = true
	else:
		$Picker/Area2D.monitorable = false

func _on_change_wave(wave_nth : int):
	if wave_nth == 2:
		get_node(cur_wave)._wave2()
	elif wave_nth == 3:
		get_node(cur_wave)._wave3()
	elif wave_nth == 4:
		get_node(cur_wave)._boss()

func _on_wave_end():
	match cur_wave:
		"Orcs_Wave":
			$Map.get_node(undeads_door).call_deferred("free")
		"Undeads_Wave":
			$Map.get_node(demons_door).call_deferred("free")
		"Demons_Wave":
			emit_signal("game_over")

func _update_children():
	# warning-ignore:unassigned_variable
	var wave_enemies_aux : Array
	
	for i in range(get_node(cur_wave).get_children().size()):
		wave_enemies_aux.push_back(get_node(cur_wave).get_child(i))
	
	wave_enemies = wave_enemies_aux

func _move_cam_x(body):
	if body.is_in_group("Hero"):
		for note in $Notes.get_children():
				note.call_deferred("free")
		
		match cur_area:
			0: 
				var dest : Vector2 = Vector2(OS.window_size.x, 0)
				
				$Camera.global_position += dest
				$Picker.global_position += dest
				$Life.rect_position     += dest
				note_spawn              += dest
				
				cur_area = areas.UNDEADS 
			1:
				var dest : Vector2 = Vector2(OS.window_size.x, 0)
				
				$Camera.global_position -= dest
				$Picker.global_position -= dest
				$Life.rect_position     -= dest
				note_spawn              -= dest
				
				cur_area = areas.ORCS
			2:
				pass

func _move_cam_y(body):
	if body.is_in_group("Hero"):
		for note in $Notes.get_children():
				note.call_deferred("free")
		
		match cur_area:
			0: 
				var dest : Vector2 = Vector2(0, -OS.window_size.y)
				
				$Camera.global_position += dest
				$Picker.global_position += dest
				$Life.rect_position     += dest
				note_spawn              += dest
				
				cur_area = areas.DEMONS
			1:
				pass
			2:
				var dest : Vector2 = Vector2(0, -OS.window_size.y)
				
				$Camera.global_position -= dest
				$Picker.global_position -= dest
				$Life.rect_position     -= dest
				note_spawn              -= dest
				
				cur_area = areas.ORCS

func _on_update_hero_pos():
	_update_children()
	
	for i in range(wave_enemies.size()):
		wave_enemies[i]._update($Hero.get_global_position())

func _change_enemies():
	match cur_wave:
		"Orcs_Wave":
			var undeads_wave  = undeads_wave_scene.instance()
			var undeads_music = undeads_music_scene.instance()
			# warning-ignore:unassigned_variable
			var enemy_pos_aux : Array
			
			for note in $Notes.get_children():
				note.call_deferred("free")
			
			cur_pointers = "Undeads_Spawn"
			var spawn_points = get_node("Map").get_node(cur_pointers).get_children()
			
			for child in self.get_children(): 
				if child.is_in_group("Music"):
					child.get_node("AnimationPlayer").play("Fade")
					yield(child.get_node("AnimationPlayer"), "animation_finished")
					
					child.call_deferred("free")
				if child.is_in_group("Wave"):
					child.call_deferred("free")
			
			cur_music = "Undeads_Music"
			undeads_music.set_name(cur_music)
			undeads_music.connect("create_note", self, "_on_create_note")
			undeads_music.connect("finished", self, "_on_finished")
			cur_spb = undeads_music._get_spb()
			
			cur_wave = "Undeads_Wave"
			undeads_wave.set_name(cur_wave)
			undeads_wave.connect("change_wave", self, "_on_change_wave")
			undeads_wave.connect("end_wave", self, "_on_wave_end")
			
			self.add_child(undeads_music)
			
			undeads_music._play_with_offset(note_offset)
			
			for i in range(spawn_points.size()):
				enemy_pos_aux.push_back(spawn_points[i])
			
			enemy_positions = enemy_pos_aux
			
			undeads_wave._setup(enemy_positions, $Hero, map_nav2d, cur_spb)
			
			self.add_child(undeads_wave, 0)
			
			get_node(cur_wave)._wave1()
			_update_children()
			
			$Life._restore()
			
			for i in range(wave_enemies.size()):
				wave_enemies[i].connect("enemy_dead", self, "_on_enemy_death")
				wave_enemies[i]._update($Hero.get_global_position())
		"Undeads_Wave":
			var demons_wave   = demons_wave_scene.instance()
			var demons_music  = demons_music_scene.instance()
			# warning-ignore:unassigned_variable
			var enemy_pos_aux : Array
			
			for note in $Notes.get_children():
				note.call_deferred("free")
			
			cur_pointers = "Demons_Spawn"
			var spawn_points = get_node("Map").get_node(cur_pointers).get_children()
			
			for child in self.get_children(): 
				if child.is_in_group("Music"):
					child.get_node("AnimationPlayer").play("Fade")
					yield(child.get_node("AnimationPlayer"), "animation_finished")
					
					child.call_deferred("free")
				if child.is_in_group("Wave"):
					child.call_deferred("free")
			
			cur_music = "Demons_Music"
			demons_music.set_name(cur_music)
			demons_music.connect("create_note", self, "_on_create_note")
			demons_music.connect("finished", self, "_on_finished")
			cur_spb = demons_music._get_spb()
			
			cur_wave = "Demons_Wave"
			demons_wave.set_name(cur_wave)
			demons_wave.connect("change_wave", self, "_on_change_wave")
			demons_wave.connect("end_wave", self, "_on_wave_end")
			
			self.add_child(demons_music)
			
			demons_music._play_with_offset(note_offset)
			
			for i in range(spawn_points.size()):
				enemy_pos_aux.push_back(spawn_points[i])
			
			enemy_positions = enemy_pos_aux
			
			demons_wave._setup(enemy_positions, $Hero, map_nav2d, cur_spb)
			
			self.add_child(demons_wave, 0)
			
			get_node(cur_wave)._wave1()
			_update_children()
			
			$Life._restore()
			
			for i in range(wave_enemies.size()):
				wave_enemies[i].connect("enemy_dead", self, "_on_enemy_death")
				wave_enemies[i]._update($Hero.get_global_position())
		"Demons_Wave":
			pass

func _on_spawn_area(body):
	if body.is_in_group("Hero"):
		match cur_area:
			0:
				pass
			1:
				_change_enemies()
				undeads_area.call_deferred("free")
			2:
				_change_enemies()
				demons_area.call_deferred("free")

func _on_create_note():
	var note = note_scene.instance()
	
	note.set_position(note_spawn)
	note._setup($Picker.get_global_position().x, note_spawn.x, note_offset, cur_spb)
	note.connect("mov", self.get_node("Hero"), "_on_mov")
	
	$Notes.add_child(note)

func _on_finished():
	match cur_music:
		"Orcs_Music":
			var orcs_music = orcs_music_scene.instance()
			
			for child in self.get_children(): 
				if child.is_in_group("Music"):
					child.call_deferred("free")
			
			orcs_music.set_name(cur_music)
			orcs_music.connect("create_note", self, "_on_create_note")
			orcs_music.connect("finished", self, "_on_finished")
			
			self.add_child(orcs_music)
			
			orcs_music._play_with_offset(note_offset)
		"Undeads_Music":
			var undeads_music = undeads_music_scene.instance()
			
			for child in self.get_children(): 
				if child.is_in_group("Music"):
					child.call_deferred("free")
			
			undeads_music.set_name(cur_music)
			undeads_music.connect("create_note", self, "_on_create_note")
			undeads_music.connect("finished", self, "_on_finished")
			
			self.add_child(undeads_music)
			
			undeads_music._play_with_offset(note_offset)
		"Demons_Music":
			var demons_music = demons_music_scene.instance()
			
			for i in range($Notes.get_children().size()):
				$Notes.get_child(i).call_deferred("free")
			
			for child in self.get_children(): 
				if child.is_in_group("Music"):
					child.call_deferred("free")
			
			demons_music.set_name(cur_music)
			demons_music.connect("create_note", self, "_on_create_note")
			demons_music.connect("finished", self, "_on_finished")
			
			self.add_child(demons_music)
			
			demons_music._play_with_offset(note_offset)

func _on_enemy_death():
	_update_children()

func _on_hero_damaged():
	$Life._heart_take_dmg()

func _on_hero_death():
	emit_signal("game_over")
