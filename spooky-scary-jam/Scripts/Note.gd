extends Node2D

enum type {NONE, PERFECT, GOOD, OKAY}

var hit   : int     = type.NONE
var speed : float
var slide : bool    = false
var pdir  : Vector2 
var inst  : int

signal mov(instruction, allow)

func _setup(picker_pos : float, init_pos_x : float, offset : int, sec_per_beat : float):
	speed = (picker_pos - init_pos_x)/((offset) * sec_per_beat)

func _physics_process(delta):
#	if slide:
	self.position.x += speed * delta
	
	if Input.is_action_just_pressed("ui_right"):
		inst = 0
	elif Input.is_action_just_pressed("ui_left"):
		inst = 1
	elif Input.is_action_just_pressed("ui_up"):
		inst = 2
	elif Input.is_action_just_pressed("ui_down"):
		inst = 3
	elif Input.is_action_just_pressed("attack"):
		inst = 4

func _hit():
	$Timer.start()
	emit_signal("mov", inst, true)
	self.visible = false

func _on_Okay_area_entered(area):
	if area.is_in_group("Picker_hit") and hit == type.NONE:
		hit = type.OKAY
		_hit()

func _on_Timer_timeout():
	hit = type.NONE
#	emit_signal("mov", Vector2.ZERO, false)
	self.call_deferred("free")
