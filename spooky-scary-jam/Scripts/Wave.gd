extends Node2D

class_name enemy_type_wave

export(PackedScene) var enemy1_scene
export(PackedScene) var enemy2_scene
export(PackedScene) var enemy3_scene
export(PackedScene) var enemy4_scene
export(PackedScene) var boss_scene

var enemy1_qty : int = 11
var enemy2_qty : int = 4
var enemy3_qty : int = 4
var enemy4_qty : int = 4
var boss_qty   : int = 1

var wave_qty  : int
var wave1_qty : int
var wave2_qty : int
var wave3_qty : int

var e1_ar : Array
var e2_ar : Array
var e3_ar : Array
var e4_ar : Array

var wave_ar : Array 
var pos_ar  : Array

var cur_enemies_dead : int
var cur_wave         : int

onready var player       : KinematicBody2D
onready var mapnav       : Navigation2D   
onready var bps          : float          

signal change_wave(wave_nth)
signal end_wave()

func _setup(positions : Array, hero : KinematicBody2D, navigation : Navigation2D, beats_per_sec : float):
	pos_ar   = positions
	player   = hero
	mapnav   = navigation
	bps      = beats_per_sec
	cur_wave = 1

func _ready():
	for _i in range(enemy1_qty):
		var e1 = enemy1_scene.instance()
		
		e1_ar.push_back(e1)
	
	for _i in range(enemy2_qty):
		var e2 = enemy2_scene.instance()
		
		e2_ar.push_back(e2)
	
	for _i in range(enemy3_qty):
		var e3 = enemy3_scene.instance()
		
		e3_ar.push_back(e3)
	
	for _i in range(enemy4_qty):
		var e4 = enemy4_scene.instance()
		
		e4_ar.push_back(e4)

func _wave1():
	var pos_iter
	
	wave_qty         = 0
	cur_enemies_dead = 0
	
	for i in range(3):
		e1_ar[0].set_position(pos_ar[i].get_global_position())
		e1_ar[0].connect("enemy_dead", self, "_on_enemy_dead")
		e1_ar[0]._setup(player, mapnav, bps)
		
		self.call_deferred("add_child", e1_ar[0])
		e1_ar.remove(0)
		
		### Get positions from [4, 6]
		e2_ar[0].set_position(pos_ar[i + 3].get_global_position())
		e2_ar[0].connect("enemy_dead", self, "_on_enemy_dead")
		e2_ar[0]._setup(player, mapnav, bps)
		
		self.call_deferred("add_child", e2_ar[0])
		e2_ar.remove(0)
		
		pos_iter = i + 3
	
	### In order to have one variable to count enemies on field
	pos_iter += 1
	wave_qty = pos_iter

func _wave2():
	var pos_iter
	
	wave_qty         = 0
	cur_enemies_dead = 0
	
	for i in range(3):
		e1_ar[0].set_position(pos_ar[i].get_global_position())
		e1_ar[0].connect("enemy_dead", self, "_on_enemy_dead")
		e1_ar[0]._setup(player, mapnav, bps)
		
		self.call_deferred("add_child", e1_ar[0])
		e1_ar.remove(0)
		
		### Get positions from [4, 6]
		e3_ar[0].set_position(pos_ar[i + 3].get_global_position())
		e3_ar[0].connect("enemy_dead", self, "_on_enemy_dead")
		e3_ar[0]._setup(player, mapnav, bps)
		
		self.call_deferred("add_child", e3_ar[0])
		e3_ar.remove(0)
		
		pos_iter = i + 3
	
	### In order to have one variable to count enemies on field
	pos_iter += 1
	wave_qty = pos_iter

func _wave3():
	var pos_iter
	
	wave_qty         = 0
	cur_enemies_dead = 0
	
	for i in range(3):
		e1_ar[0].set_position(pos_ar[i].get_global_position())
		e1_ar[0].connect("enemy_dead", self, "_on_enemy_dead")
		e1_ar[0]._setup(player, mapnav, bps)
		
		self.call_deferred("add_child", e1_ar[0])
		e1_ar.remove(0)
		
		### Get positions from [4, 6]
		e4_ar[0].set_position(pos_ar[i + 3].get_global_position())
		e4_ar[0].connect("enemy_dead", self, "_on_enemy_dead")
		e4_ar[0]._setup(player, mapnav, bps)
		
		self.call_deferred("add_child", e4_ar[0])
		e4_ar.remove(0)
		
		pos_iter = i + 3
	
	### In order to have one variable to count enemies on field
	pos_iter += 1
	wave_qty = pos_iter

func _boss():
	var pos_iter
	var boss = boss_scene.instance()
	
	wave_qty         = 0
	cur_enemies_dead = 0
	
	for i in range(2):
		e1_ar[0].set_position(pos_ar[i].get_global_position())
		e1_ar[0].connect("enemy_dead", self, "_on_enemy_dead")
		e1_ar[0]._setup(player, mapnav, bps)
		
		self.call_deferred("add_child", e1_ar[0])
		e1_ar.remove(0)
		
		pos_iter = i + 1
	
	e2_ar[0].set_position(pos_ar[pos_iter].get_global_position())
	e2_ar[0].connect("enemy_dead", self, "_on_enemy_dead")
	
	pos_iter += 1
	e3_ar[0].set_position(pos_ar[pos_iter].get_global_position())
	e3_ar[0].connect("enemy_dead", self, "_on_enemy_dead")
	
	pos_iter += 1
	e4_ar[0].set_position(pos_ar[pos_iter].get_global_position())
	e4_ar[0].connect("enemy_dead", self, "_on_enemy_dead")
	
	pos_iter += 1
	boss.set_position(pos_ar[pos_iter].get_global_position())
	boss.connect("enemy_dead", self, "_on_enemy_dead")
	
	e2_ar[0]._setup(player, mapnav, bps)
	e3_ar[0]._setup(player, mapnav, bps)
	e4_ar[0]._setup(player, mapnav, bps)
	boss._setup(player, mapnav, bps)
	
	self.call_deferred("add_child", e2_ar[0])
	self.call_deferred("add_child", e3_ar[0])
	self.call_deferred("add_child", e4_ar[0])
	self.call_deferred("add_child", boss)
	
	e2_ar.remove(0)
	e3_ar.remove(0)
	e4_ar.remove(0)
	
	pos_iter += 1
	wave_qty  = pos_iter

func _on_enemy_dead():
#	self.get_parent()._update_children()
	
	cur_enemies_dead += 1
	
	if cur_enemies_dead == wave_qty and cur_wave < 4:
		cur_wave += 1
		
		emit_signal("change_wave", cur_wave)
	elif cur_enemies_dead == wave_qty and cur_wave >= 4:
		emit_signal("end_wave")
