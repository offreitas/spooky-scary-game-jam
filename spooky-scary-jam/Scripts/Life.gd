extends HBoxContainer

export(PackedScene) var heart_scene

var hearts_num     : int = 5
var hearts_counter : int = 0
var hearts_ar      : Array
var hearts_hp      : Array
var hearts_pos     : Vector2 = Vector2(24, 0)


signal hero_dead()

func _ready():
	for i in range(hearts_num):
		var hearts : Node2D = heart_scene.instance()
		
		hearts.set_name("Heart" + str(hearts_counter))
		hearts.set_position(hearts_pos * (i + 1))
		hearts.get_node("Empty").visible = false
		hearts.get_node("Half").visible  = false
		hearts.get_node("Full").visible  = true
		
		self.add_child(hearts)
		
		hearts_counter = i
	
	hearts_ar = get_children()

func _heart_take_dmg():
	var cur_heart = hearts_ar[hearts_counter]
	var empty     = cur_heart.get_node("Empty")
	var half      = cur_heart.get_node("Half")
	var full      = cur_heart.get_node("Full")
	
	if full.is_visible():
		full.visible = false
		half.visible = true
	elif half.is_visible() and hearts_counter > 0:
		half.visible  = false
		empty.visible = true
		
		hearts_counter -= 1
	elif half.is_visible() and hearts_counter == 0:
		emit_signal("hero_dead")

func _restore():
	for i in range(hearts_num):
		var cur_heart = hearts_ar[i]
		var empty     = cur_heart.get_node("Empty")
		var half      = cur_heart.get_node("Half")
		var full      = cur_heart.get_node("Full")
		
		empty.visible = false
		half.visible  = false
		full.visible  = true
	
	hearts_counter = hearts_num - 1
